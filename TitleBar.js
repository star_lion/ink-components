'use strict';
const React = require('react')
const {Text, Box} = require('ink')
const hotkeys = require('node-hotkeys')

const {useState} = React

process.on('exit', () => {
  hotkeys.iohook.unload()
})

const TitleBar = ({title='Title', quitHotkeys='alt + q', quitText='[Alt-Q]uit'}) => {
  const [isQuitResolved, setQuitResolved] = useState(false)
  if (isQuitResolved == false) {
    hotkeys.on({
      hotkeys: quitHotkeys,
      matchAllModifiers: true,
      callback: () => {
        process.exit()
      }
    })

    setQuitResolved(true)
  }

  return (
    <>
      <Text>{title}</Text>
      <Text>{quitText}</Text>
    </>
  )
}

module.exports = TitleBar
