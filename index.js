'use strict';
const React = require('react')

const importJsx = require('import-jsx')
const TitleBar = importJsx('./TitleBar')
const LogWindow = importJsx('./LogWindow')

module.exports = {
  TitleBar,
  LogWindow
}
