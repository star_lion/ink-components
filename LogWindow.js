'use strict';
const React = require('react')
const {Text, Box, useFocus, useInput} = require('ink')

const LogWindow = ({logs=[], displayCount=3}) => {
  return (
    <Box
      justifyContent='column'
    >
      {logs.slice(displayCount * -1).map((log) => (
        <Text>{log}</Text>
      ))}
    </Box>
  )
}

module.exports = LogWindow
